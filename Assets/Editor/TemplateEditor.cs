﻿using Assets.Scripts;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof(Template))]
    public class TemplateEditor : UnityEditor.Editor
    {
        private const int _GRID_SIZE = 25;
        void OnSceneGUI()
        {
            var template = (Template) target;

            foreach (var placeHolder in template.GetComponentsInChildren<PlaceHolder>())
            {
                var holderPosition = placeHolder.transform.position;
                Handles.DrawDottedLine(holderPosition, holderPosition + Vector3.forward * 2f + Vector3.right * 2f, 5f);
                Handles.DrawDottedLine(holderPosition + Vector3.right * 2f, holderPosition + Vector3.forward * 2f, 5f);
                Handles.Label(holderPosition + Vector3.forward + Vector3.right, placeHolder.WorldItem.ToString(), new GUIStyle { fontStyle = FontStyle.Bold });
                placeHolder.transform.position = Handles.PositionHandle(holderPosition, Quaternion.identity);
            }

            for (var i = 0; i < _GRID_SIZE; i++)
            {
                Handles.DrawDottedLine(Vector3.forward * 2f * i, Vector3.forward * 2f * i + Vector3.right * 2f * _GRID_SIZE , 1f);
                Handles.DrawDottedLine(Vector3.right * 2f * i, Vector3.right * 2f * i + Vector3.forward * 2f * _GRID_SIZE , 1f);
            }
        }
    }
}
