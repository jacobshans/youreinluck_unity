﻿using Assets.Scripts;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ProcGenVisualizer))]
public class ProcGenEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var myTarget = (ProcGenVisualizer)target;

        if (GUILayout.Button("Clear")) myTarget.Clear();
        if (GUILayout.Button("Generate")) myTarget.Visualize();
    }
}