﻿using UnityEngine;

namespace Assets.Scripts
{
    public class XpPoints : MonoBehaviour, ICanTakeDamage
    {
        private float _LastTime = -_TIME_BETWEEN_SHOUTS;
        private const float _TIME_BETWEEN_SHOUTS = 5f;

        private void XpGained()
        {
            XpManager.Instance.AddRatioXp(0.5f);
            Destroy(gameObject);
        }

        public void TakeDamage(float damage)
        {
            XpGained();
        }

        private void FixedUpdate()
        {
            if (_LastTime + _TIME_BETWEEN_SHOUTS > Time.time) return;
            _LastTime = Time.time;
            var playerPosition = PlayerInput.Instance.transform.position;
            var direction = playerPosition - transform.position;

            if (direction.magnitude < 30f) PlaySound(direction.magnitude);
        }

        private void PlaySound(float directionMagnitude)
        {
            MessageKit<float>.Post(MessageType.XpPointsClose, Mathf.Min(2f / directionMagnitude + 0.5f, 2f));
        }
    }
}
