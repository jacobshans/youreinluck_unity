﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class PlayerInput : Character
    {
        public static PlayerInput Instance;
        private Controls _Controls;

        protected override void Awake()
        {
            Instance = this;
            CharacterState = new CharacterState(new CharacterStateInformation
            {
                MovementSpeed = 0.1f,
                MaxHealth = 5,
                FreezeTime = 1f,
                ChanceOnExtraBullet = 1f,
                ShootCoolDown = 0.5f,
                DamagePerBullet = .6f,
                BulletType = WorldType.Swamp
            });
            base.Awake();
            _Controls = new Controls();
            _Controls.Main.Movement.performed += context =>
            {
                var movement2d = context.ReadValue<Vector2>();
                Movement = new Vector3(movement2d.x, 0, movement2d.y);
            };
            _Controls.Main.Movement.canceled += context => Movement = Vector2.zero;
            _Controls.Main.ShootDirection.performed += context => ShootDirection = context.ReadValue<Vector2>();
        }

        protected override void Die()
        {
            CharacterShoot.enabled = false;
            CharacterMove.enabled = false;
            Invoke(nameof(ReloadScene), 2f);
        }
        private void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private void OnDisable()
        {
            _Controls.Disable();
        }

        private void OnEnable()
        {
            _Controls.Enable();
        }
    }
}