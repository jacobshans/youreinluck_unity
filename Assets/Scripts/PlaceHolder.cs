﻿using UnityEngine;

namespace Assets.Scripts
{
    public class PlaceHolder : MonoBehaviour
    {
        public WorldItem WorldItem;
    }

    public enum WorldItem
    {
        Tree,
        Rock,
        Chest,
        SmallEnemy,
        Empty,
        DoesntMatter,
        Bush,
        XpPoints
    }
}