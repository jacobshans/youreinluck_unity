﻿using System;

namespace Assets.Scripts.Modifications
{
    public class LowerShootCooldownModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.ShootCoolDown *= 0.95f;
        }

        public string GetText()
        {
            return "Reduce shoot cooldown by 5%";
        }
    }
}