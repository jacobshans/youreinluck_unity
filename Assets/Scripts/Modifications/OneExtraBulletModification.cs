﻿namespace Assets.Scripts.Modifications
{
    public class OneExtraBulletModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.ChanceOnExtraBullet += 0.2f;
        }

        public string GetText()
        {
            return "Increased chance on an extra bullet every shot";
        }
    }
}