﻿namespace Assets.Scripts.Modifications
{
    public class MoveSpeedModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.MovementSpeed *= 1.05f;
        }

        public string GetText()
        {
            return "Movement speed up 5%";
        }
    }
}