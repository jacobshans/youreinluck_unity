﻿namespace Assets.Scripts.Modifications
{
    public class MaxHealthModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.MaxHealth *= 1.1f;
            MessageKit.Post(MessageType.HealthChanged);
        }

        public string GetText()
        {
            return "Max health up 10%";
        }
    }
}