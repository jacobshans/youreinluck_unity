﻿namespace Assets.Scripts.Modifications
{
    public class IceBulletsModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.BulletType = WorldType.Ice;
        }

        public string GetText()
        {
            return "All your bullets freeze";
        }
    }
}