﻿namespace Assets.Scripts.Modifications
{
    public class FireBulletsModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.BulletType = WorldType.Lava;
        }

        public string GetText()
        {
            return "All your bullets burn";
        }
    }
}