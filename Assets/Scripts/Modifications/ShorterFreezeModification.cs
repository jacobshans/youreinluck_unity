﻿namespace Assets.Scripts.Modifications
{
    public class ShorterFreezeModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.FreezeTime *= 0.9f;
        }

        public string GetText()
        {
            return "Reduces freeze time by 10%";
        }
    }
}