﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Modifications
{
    public class ModificationApplier : MonoBehaviour
    {
        private List<IModification> _PossibleModifications = new List<IModification>();
        [SerializeField] private Text _Up;
        [SerializeField] private Text _Right;
        [SerializeField] private Text _Down;
        [SerializeField] private Text _Left;
        private Controls _Controls;
        private static ModificationApplier _OpenModificationApplier;
        

        protected void Awake()
        {
            if (_OpenModificationApplier != null) _OpenModificationApplier.ApplyModification(Random.Range(0, 4));
            _OpenModificationApplier = this;
            _Controls = new Controls();
            _Controls.Main.ModificationUp.performed += context => ApplyModification(0);
            _Controls.Main.ModificationRight.performed += context => ApplyModification(1);
            _Controls.Main.ModificationDown.performed += context => ApplyModification(2);
            _Controls.Main.ModificationLeft.performed += context => ApplyModification(3);
            var allPossibleModifications = GetAllPossibleModifications();
            _PossibleModifications = GetPossibleModifications(allPossibleModifications);
        }

        private void ApplyModification(int i)
        {
            PlayerInput.Instance.ApplyModification(_PossibleModifications[i]);
            _OpenModificationApplier = null;
            Destroy(gameObject);
        }

        private List<IModification> GetPossibleModifications(List<Type> allPossibleModifications)
        {
            var mods = new List<IModification>();
            for (var i = 0; i < 4; i++)
            {
                var modType = allPossibleModifications[Random.Range(0, allPossibleModifications.Count)];
                var mod = (IModification)Activator.CreateInstance(modType);
                allPossibleModifications.Remove(modType);
                SetTextForMod(i, mod);
                mods.Add(mod);
            }

            return mods;
        }

        private void SetTextForMod(int i, IModification mod)
        {
            var textField = i switch
            {
                0 => _Up,
                1 => _Right,
                2 => _Down,
                3 => _Left,
                _ => throw new NotSupportedException()
            };

            textField.text = mod.GetText();
        }

        public static List<Type> GetAllPossibleModifications()
        {
            return new List<Type>
            {
                typeof(MoveSpeedModification),
                typeof(MaxHealthModification),
                typeof(ShorterFreezeModification),
                typeof(OneExtraBulletModification),
                typeof(LowerShootCooldownModification),
                typeof(HigherDamagePerBulletModification),
                typeof(IceBulletsModification),
                typeof(FireBulletsModification)
            };
        }

        private void OnDisable()
        {
            _Controls.Disable();
        }

        private void OnEnable()
        {
            _Controls.Enable();
        }
    }
}
