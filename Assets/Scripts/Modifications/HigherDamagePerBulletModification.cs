﻿namespace Assets.Scripts.Modifications
{
    public class HigherDamagePerBulletModification : IModification
    {
        public void ApplyModification(CharacterStateInformation information)
        {
            information.DamagePerBullet *= 1.1f;
        }

        public string GetText()
        {
            return "10% higher damage per bullet";
        }
    }
}