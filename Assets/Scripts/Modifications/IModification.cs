﻿namespace Assets.Scripts.Modifications
{
    public interface IModification
    {
        void ApplyModification(CharacterStateInformation information);
        string GetText();
    }
}