﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Modifications;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class Enemy : Character
    {
        private Transform _PlayerTransform;
        private const float _NOTICE_DISTANCE = 10f;
        private Vector3 _AggregatePlayerPosition;
        private const float _XP_PER_LEVEL_PER_ENEMY = 10f;

        private static List<(int x, int z)> _KilledEnemies = new List<(int x, int z)>();
        private static List<(int x, int z)> _AliveEnemies = new List<(int x, int z)>();

        private int _Level;

        protected override void Awake()
        {
            CharacterState =
                new CharacterState(new CharacterStateInformation
                {
                    MovementSpeed = 0.1f,
                    MaxHealth = 2,
                    FreezeTime = 1f,
                    ChanceOnExtraBullet = .7f,
                    ShootCoolDown = 1f,
                    DamagePerBullet = 1,
                    BulletType = WorldType.Swamp
                });
            base.Awake();
            _PlayerTransform = PlayerInput.Instance.transform;
            if (_KilledEnemies.Any(enemy => enemy.x == transform.position.x && enemy.z == transform.position.z)) Destroy(gameObject);
            else if (_AliveEnemies.Any(enemy => enemy.x == transform.position.x && enemy.z == transform.position.z)) Destroy(gameObject);
            else
            {
                _AliveEnemies.Add(((int)transform.position.x, (int)transform.position.z));
            }
        }
        protected override void Start()
        {
            base.Start();
            SetLevel(PlayerInput.Instance.GetLevel());
            HandleWorldType();
        }

        public void HandleWorldType()
        {
            var worldType = WorldDeterminer.GetWorldTypeForLocation(transform.position);
            switch (worldType)
            {
                case WorldType.Ice:
                    CharacterState.ApplyModification(new IceBulletsModification());
                    break;
                case WorldType.Swamp:
                    break;
                case WorldType.Desert:
                    break;
                case WorldType.Lava:
                    CharacterState.ApplyModification(new FireBulletsModification());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetLevel(int level)
        {
            _Level = level;
            CharacterCanvas.UpdateLevel(level);
            var allPossibleModifications = ModificationApplier.GetAllPossibleModifications();
            for (var i = 0; i < level; i++)
            {
                var randomModificationType = allPossibleModifications[Random.Range(0, allPossibleModifications.Count)];
                CharacterState.ApplyModification((IModification)Activator.CreateInstance(randomModificationType));
                allPossibleModifications.Remove(randomModificationType);
            }
        }

        protected override void FixedUpdate()
        {
            _AggregatePlayerPosition = _AggregatePlayerPosition * 0.9f + _PlayerTransform.position * 0.1f;
            var direction = _AggregatePlayerPosition - transform.position;
            if ((_PlayerTransform.position - transform.position).magnitude > 70f) GoAway();
            if (direction.magnitude > _NOTICE_DISTANCE)
            {
                Movement = Vector2.zero;
                ShootDirection = Vector2.zero;
                return;
            }

            if (direction.magnitude < 3f &&
                CharacterState.GetInformation().ChanceOnExtraBullet > 0.8f &&
                (Math.Abs(direction.x) < 0.1f || Math.Abs(direction.z) < 0.1f))
            {
                direction = Vector2.zero;
            }
            ShootDirection = new Vector2(direction.x, direction.y);
            Movement = new Vector3(direction.x, 0f, direction.z).normalized;
            base.FixedUpdate();
        }

        private void OnControllerColliderHit(ControllerColliderHit colliderHit)
        {
            if (!colliderHit.gameObject.HasParentWhereNameContains("Player")) return;
            colliderHit.gameObject.GetComponentInParent<PlayerInput>().TakeDamage(.1f);
        }

        protected override void Die()
        {
            XpManager.Instance.AddXp(_Level * _XP_PER_LEVEL_PER_ENEMY);
            _KilledEnemies.Add(((int)transform.position.x, (int)transform.position.z));
            _AliveEnemies.Remove(((int)transform.position.x, (int)transform.position.z));
            Destroy(gameObject);
        }

        private void GoAway()
        {
            _AliveEnemies.Remove(((int)transform.position.x, (int)transform.position.z));
            Destroy(gameObject);
        }

        public static void ResetPersistentState()
        {
            _KilledEnemies = new List<(int x, int z)>();
            _AliveEnemies = new List<(int x, int z)>();
        }
    }
}