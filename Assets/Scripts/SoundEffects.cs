﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class SoundEffects : MonoBehaviour
    {
        [SerializeField] private AudioSource _BulletsFiredSource;
        [SerializeField] private AudioSource _EnemyHurtSource;
        [SerializeField] private AudioSource _LevelIncreasedSource;
        [SerializeField] private AudioSource _PlayerHurtSource;
        [SerializeField] private AudioClip[] _RandomXpAudioClips;
        [SerializeField] private AudioSource _XpPointsCloseSource;
        
        private void Start()
        {
            MessageKit.AddObserver(MessageType.BulletsFired, () => { PlayAudio(_BulletsFiredSource); });
            MessageKit.AddObserver(MessageType.EnemyHurt, () => { PlayAudio(_EnemyHurtSource); });
            MessageKit.AddObserver(MessageType.PlayerHurt, () => { PlayAudio(_PlayerHurtSource); });
            MessageKit.AddObserver(MessageType.LevelIncreased, () => { PlayAudio(_LevelIncreasedSource); });
            MessageKit<float>.AddObserver(MessageType.XpPointsClose, volume => { PlayRandomAudio(_XpPointsCloseSource, volume); });
        }

        private float GetPitchForAudioClip(AudioSource audioClip)
        {
            Enum.TryParse(audioClip.name, out MessageType messageType);
            return messageType switch
            {
                MessageType.BulletsFired => Random.Range(2f, 3f),
                MessageType.PlayerHurt => Random.Range(0.9f, 1.1f),
                MessageType.EnemyHurt => Random.Range(1.3f, 1.8f),
                MessageType.LevelIncreased => 1f,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        private float GetVolumeForAudioClip(AudioSource audioClip)
        {
            Enum.TryParse(audioClip.name, out MessageType messageType);
            return messageType switch
            {
                MessageType.BulletsFired => Random.Range(0.5f, .8f),
                MessageType.PlayerHurt => Random.Range(1.7f, 2.2f),
                MessageType.EnemyHurt => Random.Range(0.7f, 1f),
                MessageType.LevelIncreased => 1f,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        private void OnDestroy()
        {
            MessageKit.ClearMessageTable();
        }

        private void PlayAudio(AudioSource audioSource)
        {
            audioSource.pitch = GetPitchForAudioClip(audioSource);
            audioSource.volume = GetVolumeForAudioClip(audioSource);
            audioSource.Play();
        }

        private void PlayRandomAudio(AudioSource audioSource, float volume)
        {
            audioSource.PlayOneShot(_RandomXpAudioClips[Random.Range(0, _RandomXpAudioClips.Length)], volume);
        }
    }
}