﻿using System.Collections.Generic;
using Assets.Scripts.Modifications;

namespace Assets.Scripts
{
    public class CharacterState
    {
        private readonly CharacterStateInformation _Information;
        private readonly List<IModification> _Modifications = new List<IModification>();

        public CharacterState(CharacterStateInformation startInformation)
        {
            _Information = startInformation;
        }

        public void ApplyModification(IModification modification)
        {
            modification.ApplyModification(_Information);
            _Modifications.Add(modification);
        }

        public CharacterStateInformation GetInformation()
        {
            return _Information;
        }
    }
}