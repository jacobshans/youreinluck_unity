﻿namespace Assets.Scripts
{
    public class CharacterStateInformation
    {
        public float MovementSpeed { get; set; }
        public float MaxHealth { get; set; }
        public float FreezeTime { get; set; }
        public float ChanceOnExtraBullet { get; set; }
        public float ShootCoolDown { get; set; }
        public float DamagePerBullet { get; set; }
        public WorldType BulletType { get; set; }
    }
}