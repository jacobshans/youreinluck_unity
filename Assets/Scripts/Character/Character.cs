﻿using Assets.Scripts.Modifications;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class Character : MonoBehaviour, ICanTakeDamage, ICanBeFrozen, ICanBeBurnt, ICharacterStateInformationHolder
    {
        [SerializeField] private Transform _ModelTransform;
        [SerializeField] private Animator _Animator;
        [SerializeField] protected CharacterCanvas CharacterCanvas;
        [SerializeField] protected MessageType HurtAudioType;
        protected CharacterMove CharacterMove;
        protected CharacterHealth CharacterHealth;
        protected CharacterShooter CharacterShoot;
        private float _LastTimeFrozen;
        protected bool IsFrozen => Time.time < _LastTimeFrozen + CharacterState.GetInformation().FreezeTime;
        protected Vector3 Movement;
        protected Vector2 ShootDirection;
        private int _Level = 0;

        protected CharacterState CharacterState;

        protected virtual void Awake()
        {
            IncreaseLevel();
        }

        protected virtual void Start()
        {
            MessageKit.AddObserver(MessageType.HealthChanged, () => CharacterCanvas.UpdateHealth(CharacterHealth.GetHealthPercentage()));
            CharacterMove = GetComponent<CharacterMove>();
            CharacterMove.SetMovementSpeed(CharacterState.GetInformation());
            CharacterHealth = GetComponent<CharacterHealth>();
            CharacterHealth.SetStartHealth(CharacterState.GetInformation());
            CharacterShoot = GetComponent<CharacterShooter>();
            CharacterShoot.SetShooterInformation(CharacterState.GetInformation());
            _LastTimeFrozen = -CharacterState.GetInformation().FreezeTime;
        }
        protected virtual void FixedUpdate()
        {
            if (IsFrozen || CharacterHealth.HasNoHealthLeft()) return;
            CharacterMove.Move(Movement);
            CharacterShoot.ShootDirection(new Vector3(ShootDirection.x, 0f, ShootDirection.y));
            if(_Animator != null) _Animator.SetBool("Walking", Movement.magnitude > 0.1f);
            _ModelTransform.rotation = Quaternion.AngleAxis(Mathf.Atan2(Movement.z, -Movement.x) * Mathf.Rad2Deg, Vector3.up);
        }

        public void TakeDamage(float damage)
        {
            if (CharacterHealth.TakeDamage(damage)) MessageKit.Post(MessageType.PlayerHurt);
            CharacterCanvas.UpdateHealth(CharacterHealth.GetHealthPercentage());
            if (CharacterHealth.HasHealthLeft()) return;
            Die();
        }

        protected abstract void Die();

        public void Freeze()
        {
            if (IsFrozen) return;
            _LastTimeFrozen = Time.time;
            var frozenBlock = Instantiate(Resources.Load("Prefabs/FrozenBlock"), transform.position, Quaternion.identity);
            Destroy(frozenBlock, CharacterState.GetInformation().FreezeTime);
        }

        public void Burn()
        {
            var fireBlock = Instantiate(Resources.Load("Prefabs/FireBlock"), transform.position, Quaternion.identity, transform);
            Destroy(fireBlock, 1.1f);
            Invoke(nameof(BurnDelayed), 1.1f);
        }

        public void BurnDelayed()
        {
            if (CharacterHealth.TakeExtraDamage(1f)) MessageKit.Post(HurtAudioType);
        }

        public int GetLevel()
        {
            return _Level;
        }

        public void IncreaseLevel()
        {
            _Level++;
            CharacterCanvas.UpdateLevel(_Level);
        }

        private void OnDestroy()
        {
            MessageKit.RemoveObserver(MessageType.HealthChanged, () => CharacterCanvas.UpdateHealth(CharacterHealth.GetHealthPercentage()));
        }

        public CharacterStateInformation GetCharacterStateInformation()
        {
            return CharacterState.GetInformation();
        }

        public void ApplyModification(IModification modification)
        {
            CharacterState.ApplyModification(modification);
        }
    }
}