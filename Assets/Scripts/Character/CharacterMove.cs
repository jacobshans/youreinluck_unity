﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CharacterMove : MonoBehaviour
    {
        private CharacterController _CharacterController;
        private CharacterStateInformation _CharacterStateInformation;

        private void Awake()
        {
            _CharacterController = GetComponent<CharacterController>();
            _CharacterController = GetComponent<CharacterController>();
        }

        public void SetMovementSpeed(CharacterStateInformation movementSpeed)
        {
            _CharacterStateInformation = movementSpeed;
        }

        public void Move(Vector3 movement)
        {
            _CharacterController.Move(movement * _CharacterStateInformation.MovementSpeed);
            transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
        }
    }
}