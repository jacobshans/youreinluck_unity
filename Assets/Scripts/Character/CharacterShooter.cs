﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class CharacterShooter : MonoBehaviour
    {
        private CharacterController _CharacterController;
        private float _LastShotTimeStamp;
        private const float _SHOOT_FROM_CHARACTER = 0.075f;
        [SerializeField] private float _ShootSpeed;
        private CharacterStateInformation _CharacterStateInformation;

        private void Awake()
        {
            _CharacterController = GetComponent<CharacterController>();
        }

        private Vector3 GetSimpleDirection(Vector3 shootDirection)
        {
            if (Math.Abs(shootDirection.x) > Math.Abs(shootDirection.z))
            {
                return shootDirection.x > 0 ? Vector3.right : Vector3.left;
            }

            return shootDirection.z > 0 ? Vector3.forward : Vector3.back;
        }

        private void Shoot(Vector3 shootDirection)
        {
            if (_CharacterStateInformation.ChanceOnExtraBullet < .8f) return;
            if (_LastShotTimeStamp + _CharacterStateInformation.ShootCoolDown > Time.time) return;

            var chanceOnExtraBullet = Random.Range(0, 1f) > _CharacterStateInformation.ChanceOnExtraBullet % 1f
                ? Mathf.Floor(_CharacterStateInformation.ChanceOnExtraBullet)
                : Mathf.Ceil(_CharacterStateInformation.ChanceOnExtraBullet);
            for (var i = 0; i < chanceOnExtraBullet; i++)
            {
                var offset = DetermineBulletOffset(chanceOnExtraBullet, i);
                ShootOneBullet(shootDirection, offset / 2f, Math.Abs(shootDirection.y) < Math.Abs(shootDirection.x));
            }
            MessageKit.Post(MessageType.BulletsFired);
        }

        private static float DetermineBulletOffset(float chanceOnExtraBullet, int i)
        {
            float offset;
            if (chanceOnExtraBullet % 2 == 1)
            {
                if (i == 0)
                    offset = 0f;
                else
                    offset = i % 2 == 1 ? i + 0.5f : -i + 0.5f;
            }
            else
            {
                offset = i % 2 == 0 ? i + 0.5f : -i - 0.5f;
            }

            return offset;
        }

        public void ShootDirection(Vector3 shootDirection)
        {
            if (shootDirection.magnitude < 0.90f) return;
            Shoot(GetSimpleDirection(shootDirection));
        }

        private void ShootOneBullet(Vector3 shootDirection, float offset, bool offsetDirection)
        {
            var bullet = (GameObject) Instantiate(Resources.Load("Prefabs/Bullet"), transform.position + Vector3.up * 0.5f + 1f*shootDirection + 0.7f * (offsetDirection ? Vector3.forward : Vector3.right) * offset, Quaternion.identity);
            var bulletScript = bullet.GetComponent<Bullet>();
            bulletScript.SetWorldType(_CharacterStateInformation.BulletType);
            bulletScript.SetDamagePower(_CharacterStateInformation.DamagePerBullet);
            var bulletRigidbody = bullet.GetComponent<Rigidbody>();
            bulletRigidbody.velocity = (GetSimpleDirection(shootDirection) + _CharacterController.velocity * _SHOOT_FROM_CHARACTER).normalized * _ShootSpeed;
            _LastShotTimeStamp = Time.time;
        }

        public void SetShooterInformation(CharacterStateInformation information)
        {
            _CharacterStateInformation = information;
            _LastShotTimeStamp = -information.ShootCoolDown;
        }
    }
}