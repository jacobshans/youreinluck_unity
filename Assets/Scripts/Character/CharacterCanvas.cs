﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class CharacterCanvas : MonoBehaviour
    {
        private Transform _CameraTransform;
        [SerializeField] private Slider _Slider;
        [SerializeField] private Text _LevelText;

        private void Awake()
        {
            _CameraTransform = Camera.main.transform;
        }

        private void Update()
        {
            transform.rotation = _CameraTransform.rotation;
        }

        public void UpdateHealth(float healthPercentage)
        {
            _Slider.value = healthPercentage;
        }

        public void UpdateLevel(int level)
        {
            _LevelText.text = $"lvl {level}";
        }
    }
}
