﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CharacterHealth : MonoBehaviour
    {
        private CharacterStateInformation _CharacterStateInformation;
        private float _Health;
        [SerializeField] private float _HitCooldown;
        private float _LastHitTimeStamp;

        public void SetStartHealth(CharacterStateInformation information)
        {
            _CharacterStateInformation = information;
            _Health = information.MaxHealth;
        }

        public bool TakeDamage(float damage)
        {
            if (_LastHitTimeStamp + _HitCooldown > Time.time) return false;
            if (HasNoHealthLeft()) return false;
            _Health -= damage;
            _LastHitTimeStamp = Time.time;
            MessageKit.Post(MessageType.HealthChanged);
            return true;
        }

        public bool TakeExtraDamage(float damage)
        {
            if (HasNoHealthLeft()) return false;
            _Health -= damage;
            return true;
        }

        private void Awake()
        {
            InvokeRepeating(nameof(AddHealth), 5, 5);
        }

        private void AddHealth()
        {
            _Health += _CharacterStateInformation.MaxHealth * 0.01f * Time.timeScale;
            _Health = Mathf.Min(_Health, _CharacterStateInformation.MaxHealth);
            MessageKit.Post(MessageType.HealthChanged);
        }

        public float GetHealth()
        {
            return _Health;
        }

        public bool HasNoHealthLeft()
        {
            return !HasHealthLeft();
        }

        public bool HasHealthLeft()
        {
            return _Health > 0;
        }

        public float GetHealthPercentage()
        {
            return _Health / _CharacterStateInformation.MaxHealth;
        }
    }
}
