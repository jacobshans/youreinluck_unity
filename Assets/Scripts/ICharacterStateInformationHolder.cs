﻿namespace Assets.Scripts
{
    public interface ICharacterStateInformationHolder
    {
        CharacterStateInformation GetCharacterStateInformation();
    }
}