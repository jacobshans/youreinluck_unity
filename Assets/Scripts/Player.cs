﻿using Assets.Scripts.Modifications;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class Player : Singleton<Player>, ICanTakeDamage, ICanBeFrozen, ICanBeBurnt, ICharacterStateInformationHolder
    {
        private CharacterMove _CharacterMove;
        private CharacterHealth _CharacterHealth;
        [SerializeField] private CharacterCanvas _CharacterCanvas;
        private CharacterShooter _CharacterShoot;
        private Controls _Controls;
        private Vector2 _Movement;
        public Vector3 EngineWorldPositionOffset { get; private set; }
        private float _LastTimeFrozen;
        private bool IsFrozen => Time.time < _LastTimeFrozen + _CharacterState.GetInformation().FreezeTime;
        [SerializeField] private Transform _ModelTransform;
        [SerializeField] private Animator _Animator;
        private readonly CharacterState _CharacterState =
            new CharacterState(new CharacterStateInformation {
                MovementSpeed = 0.1f,
                MaxHealth = 5,
                FreezeTime = 1f,
                ChanceOnExtraBullet = 1f,
                ShootCoolDown = 0.5f,
                DamagePerBullet = 1,
                BulletType = WorldType.Swamp
        });

        private int _Level = 0;
        private Vector2 _ShootDirection;
        
        protected override void Awake()
        {
            base.Awake();
            _Controls = new Controls();
            _Controls.Main.Movement.performed += context => _Movement = context.ReadValue<Vector2>();
            _Controls.Main.Movement.canceled += context => _Movement = Vector2.zero;
            _Controls.Main.ShootDirection.performed += context => _ShootDirection = context.ReadValue<Vector2>();
            IncreaseLevel();
            MessageKit.AddObserver(MessageType.HealthChanged, () => _CharacterCanvas.UpdateHealth(_CharacterHealth.GetHealthPercentage()));
        }
        private void Start()
        {
            _CharacterMove = GetComponent<CharacterMove>();
            _CharacterMove.SetMovementSpeed(_CharacterState.GetInformation());
            _CharacterHealth = GetComponent<CharacterHealth>();
            _CharacterHealth.SetStartHealth(_CharacterState.GetInformation());
            _CharacterShoot = GetComponent<CharacterShooter>();
            _CharacterShoot.SetShooterInformation(_CharacterState.GetInformation());
            _LastTimeFrozen = -_CharacterState.GetInformation().FreezeTime;
        }
        private void FixedUpdate()
        {
            if (IsFrozen || _CharacterHealth.HasNoHealthLeft() ) return;
            _CharacterMove.Move(new Vector3(_Movement.x, 0f, _Movement.y));
            _CharacterShoot.ShootDirection(_ShootDirection);
            _Animator.SetBool("Walking", _Movement.magnitude > 0.1f);
            _ModelTransform.rotation = Quaternion.AngleAxis(Mathf.Atan2(_Movement.y, -_Movement.x) * Mathf.Rad2Deg, Vector3.up);
        }

        public void TakeDamage(float damage)
        {
            if(_CharacterHealth.TakeDamage(damage)) MessageKit.Post(MessageType.PlayerHurt);
            _CharacterCanvas.UpdateHealth(_CharacterHealth.GetHealthPercentage());
            if (_CharacterHealth.HasHealthLeft()) return;
            Die();
        }

        private void Die()
        {
            _CharacterShoot.enabled = false;
            _CharacterMove.enabled = false;
            Invoke(nameof(ReloadScene), 2f);
        }

        private void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Freeze()
        {
            if (IsFrozen) return;
            _LastTimeFrozen = Time.time;
            var frozenBlock = Instantiate(Resources.Load("Prefabs/FrozenBlock"), transform.position, Quaternion.identity);
            Destroy(frozenBlock, _CharacterState.GetInformation().FreezeTime);
        }

        public void Burn()
        {
            var fireBlock = Instantiate(Resources.Load("Prefabs/FireBlock"), transform.position, Quaternion.identity, transform);
            Destroy(fireBlock, 1.1f);
            Invoke("BurnDelayed", 1.1f);
        }

        public void BurnDelayed()
        {
            if (_CharacterHealth.TakeExtraDamage(1f)) MessageKit.Post(MessageType.PlayerHurt);
        }

        public void AddOffset(Vector3 offset)
        {
            EngineWorldPositionOffset += offset;
        }

        public int GetLevel()
        {
            return _Level;
        }

        public void IncreaseLevel()
        {
            _Level++;
            _CharacterCanvas.UpdateLevel(_Level);
        }

        public CharacterStateInformation GetCharacterStateInformation()
        {
            return _CharacterState.GetInformation();
        }

        private void OnDisable()
        {
            _Controls.Disable();
        }

        private void OnEnable()
        {
            _Controls.Enable();
        }

        public void ApplyModification(IModification modification)
        {
            _CharacterState.ApplyModification(modification);
        }
    }
}