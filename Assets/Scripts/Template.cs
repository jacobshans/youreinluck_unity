﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Template : MonoBehaviour
    {
        public Dictionary<Vector3, WorldItem> GetLocations()
        {
            var itemsForLocation = new Dictionary<Vector3, WorldItem>();
            foreach (var placeHolder in GetComponentsInChildren<PlaceHolder>())
            {
                itemsForLocation.Add(placeHolder.transform.position, placeHolder.WorldItem);
            }
            return itemsForLocation;
        }
    }
}
