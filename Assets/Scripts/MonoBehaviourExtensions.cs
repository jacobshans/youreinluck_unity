﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class MonoBehaviourExtensions
    {
        public static T GetComponentOfParentWhereNameStartsWith<T>(this GameObject gameObject, string name) where T : class
        {
            var topTransform = gameObject.transform;
            while (topTransform != null)
            {
                if (topTransform.name.StartsWith(name)) return topTransform.GetComponent<T>();
                topTransform = topTransform.parent;
            }

            return null;
        }

        public static bool HasParentWhereNameContains(this GameObject gameObject, string name)
        {
            var topTransform = gameObject.transform;
            while (topTransform != null)
            {
                if (topTransform.name.Contains(name)) return true;
                topTransform = topTransform.parent;
            }

            return false;
        }
    }
}