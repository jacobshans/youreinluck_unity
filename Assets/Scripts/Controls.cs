// GENERATED AUTOMATICALLY FROM 'Assets/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Main"",
            ""id"": ""b3654b15-86e9-424e-9734-87458ac14608"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""7f366e97-b729-47be-acb7-ee392a14ce3b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShootDirection"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5a80469b-47f7-47cb-afe6-bda4811c8bf5"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ModificationUp"",
                    ""type"": ""Button"",
                    ""id"": ""e5613167-2eb1-40fc-9b2a-2161622caf3b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ModificationRight"",
                    ""type"": ""Button"",
                    ""id"": ""8f577a72-a937-460d-a907-84e34a9feaf0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ModificationDown"",
                    ""type"": ""Button"",
                    ""id"": ""2590426e-694e-478d-9098-857911bb61c2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ModificationLeft"",
                    ""type"": ""Button"",
                    ""id"": ""917ca25b-343c-4540-8dc6-5f06ac78324d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DebugCanvas"",
                    ""type"": ""Button"",
                    ""id"": ""3076d405-5558-4a70-a932-8681c9733bde"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f08ddfc0-7296-4254-ad3f-d30e725eb520"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae879fef-5b25-4f87-9ec4-483c8c78efbb"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShootDirection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fcf3e853-48cf-45e1-8620-00f051e09f37"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ModificationUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""59809498-72e8-4326-9cbb-84845d58f5e3"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ModificationRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d4aff2bc-b408-4b1a-abee-963f0043165f"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ModificationDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""27584ecd-0384-4018-8cc7-66b5ecc8f5ad"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ModificationLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6d70d4f0-c53e-49f2-b976-37a29af96c71"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DebugCanvas"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Main
        m_Main = asset.FindActionMap("Main", throwIfNotFound: true);
        m_Main_Movement = m_Main.FindAction("Movement", throwIfNotFound: true);
        m_Main_ShootDirection = m_Main.FindAction("ShootDirection", throwIfNotFound: true);
        m_Main_ModificationUp = m_Main.FindAction("ModificationUp", throwIfNotFound: true);
        m_Main_ModificationRight = m_Main.FindAction("ModificationRight", throwIfNotFound: true);
        m_Main_ModificationDown = m_Main.FindAction("ModificationDown", throwIfNotFound: true);
        m_Main_ModificationLeft = m_Main.FindAction("ModificationLeft", throwIfNotFound: true);
        m_Main_DebugCanvas = m_Main.FindAction("DebugCanvas", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Main
    private readonly InputActionMap m_Main;
    private IMainActions m_MainActionsCallbackInterface;
    private readonly InputAction m_Main_Movement;
    private readonly InputAction m_Main_ShootDirection;
    private readonly InputAction m_Main_ModificationUp;
    private readonly InputAction m_Main_ModificationRight;
    private readonly InputAction m_Main_ModificationDown;
    private readonly InputAction m_Main_ModificationLeft;
    private readonly InputAction m_Main_DebugCanvas;
    public struct MainActions
    {
        private @Controls m_Wrapper;
        public MainActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Main_Movement;
        public InputAction @ShootDirection => m_Wrapper.m_Main_ShootDirection;
        public InputAction @ModificationUp => m_Wrapper.m_Main_ModificationUp;
        public InputAction @ModificationRight => m_Wrapper.m_Main_ModificationRight;
        public InputAction @ModificationDown => m_Wrapper.m_Main_ModificationDown;
        public InputAction @ModificationLeft => m_Wrapper.m_Main_ModificationLeft;
        public InputAction @DebugCanvas => m_Wrapper.m_Main_DebugCanvas;
        public InputActionMap Get() { return m_Wrapper.m_Main; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MainActions set) { return set.Get(); }
        public void SetCallbacks(IMainActions instance)
        {
            if (m_Wrapper.m_MainActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_MainActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnMovement;
                @ShootDirection.started -= m_Wrapper.m_MainActionsCallbackInterface.OnShootDirection;
                @ShootDirection.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnShootDirection;
                @ShootDirection.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnShootDirection;
                @ModificationUp.started -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationUp;
                @ModificationUp.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationUp;
                @ModificationUp.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationUp;
                @ModificationRight.started -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationRight;
                @ModificationRight.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationRight;
                @ModificationRight.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationRight;
                @ModificationDown.started -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationDown;
                @ModificationDown.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationDown;
                @ModificationDown.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationDown;
                @ModificationLeft.started -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationLeft;
                @ModificationLeft.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationLeft;
                @ModificationLeft.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnModificationLeft;
                @DebugCanvas.started -= m_Wrapper.m_MainActionsCallbackInterface.OnDebugCanvas;
                @DebugCanvas.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnDebugCanvas;
                @DebugCanvas.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnDebugCanvas;
            }
            m_Wrapper.m_MainActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @ShootDirection.started += instance.OnShootDirection;
                @ShootDirection.performed += instance.OnShootDirection;
                @ShootDirection.canceled += instance.OnShootDirection;
                @ModificationUp.started += instance.OnModificationUp;
                @ModificationUp.performed += instance.OnModificationUp;
                @ModificationUp.canceled += instance.OnModificationUp;
                @ModificationRight.started += instance.OnModificationRight;
                @ModificationRight.performed += instance.OnModificationRight;
                @ModificationRight.canceled += instance.OnModificationRight;
                @ModificationDown.started += instance.OnModificationDown;
                @ModificationDown.performed += instance.OnModificationDown;
                @ModificationDown.canceled += instance.OnModificationDown;
                @ModificationLeft.started += instance.OnModificationLeft;
                @ModificationLeft.performed += instance.OnModificationLeft;
                @ModificationLeft.canceled += instance.OnModificationLeft;
                @DebugCanvas.started += instance.OnDebugCanvas;
                @DebugCanvas.performed += instance.OnDebugCanvas;
                @DebugCanvas.canceled += instance.OnDebugCanvas;
            }
        }
    }
    public MainActions @Main => new MainActions(this);
    public interface IMainActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnShootDirection(InputAction.CallbackContext context);
        void OnModificationUp(InputAction.CallbackContext context);
        void OnModificationRight(InputAction.CallbackContext context);
        void OnModificationDown(InputAction.CallbackContext context);
        void OnModificationLeft(InputAction.CallbackContext context);
        void OnDebugCanvas(InputAction.CallbackContext context);
    }
}
