﻿// uncomment this line to enable the MessageKitManager. It provides a single clearAllMessageTables method that
// will clear every single observer that was ever added
//#define ENABLE_MESSAGE_KIT_MANAGER

using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    /// <summary>
    /// think of MessageKit as a safe, fast replacement for SendMessage. Decoupled messages identified by an int. It is recommended to define
    /// your messages as const so they can be easily referenced and identified when you read your code (see the demo scene for an example)
    /// </summary>
    public static class MessageKit
    {
        private static readonly Dictionary<MessageType, List<Action>> MessageTable = new Dictionary<MessageType, List<Action>>();

#if ENABLE_MESSAGE_KIT_MANAGER
		static MessageKit()
		{
			MessageKitManager.registerMessageKitInstance( _messageTable );
		}
#endif

        public static void AddObserver(MessageType messageType, Action handler)
        {
            if (!MessageTable.TryGetValue(messageType, out var list))
            {
                list = new List<Action>();
                MessageTable.Add(messageType, list);
            }

            if (!list.Contains(handler))
                MessageTable[messageType].Add(handler);
        }

        public static void RemoveObserver(MessageType messageType, Action handler)
        {
            if (MessageTable.TryGetValue(messageType, out var list))
            {
                if (list.Contains(handler))
                    list.Remove(handler);
            }
        }

        public static void Post(MessageType messageType)
        {
            if (MessageTable.TryGetValue(messageType, out var list))
            {
                for (var i = list.Count - 1; i >= 0; i--)
                    list[i]();
            }
        }

        public static void ClearMessageTable(MessageType messageType)
        {
            if (MessageTable.ContainsKey(messageType))
                MessageTable.Remove(messageType);
        }

        public static void ClearMessageTable()
        {
            MessageTable.Clear();
        }
    }

    public static class MessageKit<TU>
    {
        private static Dictionary<MessageType, List<Action<TU>>> _MessageTable = new Dictionary<MessageType, List<Action<TU>>>();

#if ENABLE_MESSAGE_KIT_MANAGER
		static MessageKit()
		{
			MessageKitManager.registerMessageKitInstance( _messageTable );
		}
#endif

        public static void AddObserver(MessageType messageType, Action<TU> handler)
        {
            if (!_MessageTable.TryGetValue(messageType, out var list))
            {
                list = new List<Action<TU>>();
                _MessageTable.Add(messageType, list);
            }

            if (!list.Contains(handler))
                _MessageTable[messageType].Add(handler);
        }

        public static void RemoveObserver(MessageType messageType, Action<TU> handler)
        {
            if (_MessageTable.TryGetValue(messageType, out var list))
            {
                if (list.Contains(handler))
                    list.Remove(handler);
            }
        }

        public static void Post(MessageType messageType, TU param)
        {
            if (_MessageTable.TryGetValue(messageType, out var list))
            {
                for (var i = list.Count - 1; i >= 0; i--)
                    list[i](param);
            }
        }

        public static void ClearMessageTable(MessageType messageType)
        {
            if (_MessageTable.ContainsKey(messageType))
                _MessageTable.Remove(messageType);
        }

        public static void ClearMessageTable()
        {
            _MessageTable.Clear();
        }
    }

    public static class MessageKit<TU, TV>
    {
        private static readonly Dictionary<MessageType, List<Action<TU, TV>>> MessageTable = new Dictionary<MessageType, List<Action<TU, TV>>>();

#if ENABLE_MESSAGE_KIT_MANAGER
		static MessageKit()
		{
			MessageKitManager.registerMessageKitInstance( _messageTable );
		}
#endif

        public static void AddObserver(MessageType messageType, Action<TU, TV> handler)
        {
            if (!MessageTable.TryGetValue(messageType, out var list))
            {
                list = new List<Action<TU, TV>>();
                MessageTable.Add(messageType, list);
            }

            if (!list.Contains(handler))
                MessageTable[messageType].Add(handler);
        }

        public static void RemoveObserver(MessageType messageType, Action<TU, TV> handler)
        {
            if (MessageTable.TryGetValue(messageType, out var list))
            {
                if (list.Contains(handler))
                    list.Remove(handler);
            }
        }

        public static void Post(MessageType messageType, TU firstParam, TV secondParam)
        {
            if (MessageTable.TryGetValue(messageType, out var list))
            {
                for (var i = list.Count - 1; i >= 0; i--)
                    list[i](firstParam, secondParam);
            }
        }

        public static void ClearMessageTable(MessageType messageType)
        {
            if (MessageTable.ContainsKey(messageType))
                MessageTable.Remove(messageType);
        }

        public static void ClearMessageTable()
        {
            MessageTable.Clear();
        }
    }

#if ENABLE_MESSAGE_KIT_MANAGER
	public static class MessageKitManager
	{
		// we store a list of any MessageKits that got created so that we can clear them out when a level loads
		private static List<IDictionary> _messageKitMessageTables = new List<IDictionary>();

		public static void registerMessageKitInstance( IDictionary messageKitMessageTable )
		{
			_messageKitMessageTables.Add( messageKitMessageTable );
		}

		public static void clearAllMessageTables()
		{
			for( var i = 0; i < _messageKitMessageTables.Count; i++ )
				_messageKitMessageTables[i].Clear();
		}
	}
#endif
}