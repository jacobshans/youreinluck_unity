﻿namespace Assets.Scripts
{
    public enum MessageType
    {
        BulletsFired,
        PlayerHurt,
        EnemyHurt,
        LevelIncreased,
        XpPointsClose,
        HealthChanged
    }
}