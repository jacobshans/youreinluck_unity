﻿namespace Assets.Scripts
{
    public interface ICanBeFrozen
    {
        void Freeze();
    }
    public interface ICanBeBurnt
    {
        void Burn();
    }
}