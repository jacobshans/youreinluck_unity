﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Bullet : MonoBehaviour
    {
        private float _DamagePower;
        private WorldType _WorldType;

        private void Awake()
        {
            Destroy(gameObject, 2f);
        }

        private void OnCollisionEnter(Collision collision)
        {
            collision.gameObject.GetComponentInParent<ICanTakeDamage>()?.TakeDamage(_DamagePower);
            if(_WorldType == WorldType.Ice) collision.gameObject.GetComponentInParent<ICanBeFrozen>()?.Freeze();
            if(_WorldType == WorldType.Lava) collision.gameObject.GetComponentInParent<ICanBeBurnt>()?.Burn();
        }

        public void SetDamagePower(float damagePower)
        {
            _DamagePower = damagePower;
        }

        public void SetWorldType(WorldType worldType)
        {
            _WorldType = worldType;
        }
    }
}