﻿using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class ProcGenVisualizer : MonoBehaviour
    {
        [SerializeField] private GameObject _FirePrefab;
        [SerializeField] private GameObject _GroundPrefab;
        [SerializeField] private GameObject _IcePrefab;
        [SerializeField] private GameObject _SwampPrefab;
        [SerializeField] private Vector2 _Offset;
        [SerializeField] private float Scale;

        public void Clear()
        {
            var transforms = transform.GetComponentsInChildren<Transform>()
                .Where(trans => trans.gameObject.name.Contains("Tile"))
                .ToList();
            for (var i = 0; i < transforms.Count; i++)
            {
                DestroyImmediate(transforms[i].gameObject);
            }
        }

        public void Visualize()
        {
            Clear();
            var max = 0f;

            for (var x = -WorldDeterminer.Size; x < WorldDeterminer.Size; x++)
            {
                if (Math.Abs(x) % 16f == 8f) continue;
                for (var z = -WorldDeterminer.Size; z < WorldDeterminer.Size; z++)
                {
                    if (Math.Abs(z) % 8f == 4f) continue;
                    Instantiate(DeterminePrefabDependingOnPosition(x, z), new Vector3(x, 0, z), Quaternion.identity, transform);
                }
            }
        }

        private GameObject DeterminePrefabDependingOnPosition(float x, float z)
        {
            WorldDeterminer.SetScaleAndOffset(Scale, _Offset);
            var worldType = WorldDeterminer.GetWorldTypeForLocation(new Vector3(x, 0f, z));

            return worldType switch
            {
                WorldType.Ice => _IcePrefab,
                WorldType.Swamp => _SwampPrefab,
                WorldType.Desert => _GroundPrefab,
                WorldType.Lava => _FirePrefab,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }
}