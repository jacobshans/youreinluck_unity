﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class WorldGenerator : MonoBehaviour
    {
        [SerializeField] private float _WorldXBounds;
        [SerializeField] private float _WorldZBounds;
        [SerializeField] private GameObject _SwampTile;
        [SerializeField] private GameObject _IceTile;
        [SerializeField] private GameObject _FireTile;
        [SerializeField] private GameObject _DesertTile;
        private readonly Vector2 _Offset = new Vector2(-2f, 5.2f);
        private const float _SCALE = 0.01f;
        private List<Transform> _FloorTiles;
        private const float _TILE_SIZE = 2f;
        public const float TemplateSizeX = 25f * _TILE_SIZE;
        public const float TemplateSizeZ = 25f * _TILE_SIZE;

        private TemplateInformation[] _InformationFromTemplates;

        protected void Awake()
        {
            WorldDeterminer.SetScaleAndOffset(_SCALE, _Offset);
            InitializeTemplateInformations();
        }

        protected void Start()
        {
            InitializeZeroGround(Vector3.zero);
        }

        private void InitializeTemplateInformations()
        {
            var templatePrefabs = Resources.LoadAll<GameObject>("Prefabs/Templates");
            _InformationFromTemplates = new TemplateInformation[templatePrefabs.Length];
            for (var i = 0; i < templatePrefabs.Length; i++)
            {
                _InformationFromTemplates[i] = new TemplateInformation(templatePrefabs[i].GetComponent<Template>());
            }
        }

        public void InitializeZeroGround(Vector3 worldPosition)
        {
            _FloorTiles = new List<Transform>();
            for (var x = -_WorldXBounds; x < _WorldXBounds; x += _TILE_SIZE)
            {
                for (var z = -_WorldZBounds; z < _WorldZBounds; z += _TILE_SIZE)
                {
                    var tilePosition = new Vector3(x, 0, z);

                    var newTile = SpawnTileAndItemForPosition(tilePosition);
                    _FloorTiles.Add(newTile.transform);
                }
            }
        }

        private void Update()
        {
            UpdateTilesThatAreOutOfBounds();
        }

        private void UpdateTilesThatAreOutOfBounds()
        {
            var playerPosition = PlayerInput.Instance.transform.position;

            for (var i = 0; i < _FloorTiles.Count; i++)
            {
                var oldTile = _FloorTiles[i];
                var tilePositionRelativeToPlayer = oldTile.position - playerPosition;
                var outOfBoundsMaxX = tilePositionRelativeToPlayer.x > _WorldXBounds;
                var outOfBoundsMinX = tilePositionRelativeToPlayer.x < -_WorldXBounds;
                var outOfBoundsMaxZ = tilePositionRelativeToPlayer.z > _WorldZBounds;
                var outOfBoundsMinZ = tilePositionRelativeToPlayer.z < -_WorldZBounds;
                if (!outOfBoundsMaxX && !outOfBoundsMinX && !outOfBoundsMaxZ && !outOfBoundsMinZ) continue;

                var newPosition = oldTile.transform.position;
                if (outOfBoundsMaxX) newPosition += Vector3.left * _WorldXBounds * 2f;
                if (outOfBoundsMinX) newPosition += Vector3.right * _WorldXBounds * 2f;
                if (outOfBoundsMaxZ) newPosition += Vector3.back * _WorldZBounds * 2f;
                if (outOfBoundsMinZ) newPosition += Vector3.forward * _WorldZBounds * 2f;


                var newTile = SpawnTileAndItemForPosition(newPosition);
                _FloorTiles[i] = newTile.transform;
                Destroy(oldTile.gameObject);
            }
        }

        private GameObject SpawnTileAndItemForPosition(Vector3 newPosition)
        {
            var worldTypeForLocation = WorldDeterminer.GetWorldTypeForLocation(newPosition);
            var tilePrefabForNewPosition = GetTilePrefabForWorldType(worldTypeForLocation);
            var newTile = Instantiate(tilePrefabForNewPosition, newPosition, Quaternion.identity, transform);

            var prefabForNewPosition = GetPrefabForNewPosition(newPosition);
            if (prefabForNewPosition.item == WorldItem.DoesntMatter)
            {
                if (WorldDeterminer.ShouldPlaceTreeAtLocation(newPosition))
                {
                    prefabForNewPosition = (WorldItem.Tree, Resources.Load<GameObject>($"Prefabs/Tree/Tree_{worldTypeForLocation}"));
                }
                else if (WorldDeterminer.ShouldPlaceBushAtLocation(newPosition))
                {
                    prefabForNewPosition = (WorldItem.Bush, Resources.Load<GameObject>($"Prefabs/Bush/Bush_{worldTypeForLocation}"));
                }
                else if (WorldDeterminer.ShouldPlaceSmallEnemyAtLocation(newPosition))
                {
                    prefabForNewPosition = (WorldItem.SmallEnemy, Resources.Load<GameObject>($"Prefabs/SmallEnemy/SmallEnemy_{worldTypeForLocation}"));
                }
                else
                {
                    prefabForNewPosition = (WorldItem.Empty, null);
                }
            }
            if (prefabForNewPosition.item != WorldItem.Empty)
            {
                var worldItem = Instantiate(prefabForNewPosition.prefab, newPosition, WorldDeterminer.GetRotationForPositionAndWorldItem(newPosition, prefabForNewPosition.item), prefabForNewPosition.item == WorldItem.SmallEnemy ? null : newTile.transform);
                worldItem.transform.localScale = Vector3.one * WorldDeterminer.GetScaleForWorldItem(prefabForNewPosition.item, newPosition);
            }
            return newTile;
        }

        private (WorldItem item, GameObject prefab) GetPrefabForNewPosition(Vector3 newPosition)
        {
            var template = _InformationFromTemplates[Random.Range(0, _InformationFromTemplates.Length)];
            var templateOriginLocation = template.GetTemplateOriginForLocation(newPosition);
            var locationInTemplate = template.GetLocations()
                .Where(templateItem => templateOriginLocation + template.GetOffset(newPosition) + templateItem.Key == newPosition).ToList();
            if (!locationInTemplate.Any()) return (WorldItem.DoesntMatter, null);
            var worldTypeForLocation = WorldDeterminer.GetWorldTypeForLocation(newPosition);

            var formattableString = $"Prefabs/{locationInTemplate.First().Value}/{locationInTemplate.First().Value}_{worldTypeForLocation}";
            return (locationInTemplate.First().Value, Resources.Load<GameObject>(formattableString));
        }

        private GameObject GetTilePrefabForWorldType(WorldType worldType)
        {
            return worldType switch
            {
                WorldType.Ice => _IceTile,
                WorldType.Swamp => _SwampTile,
                WorldType.Desert => _DesertTile,
                WorldType.Lava => _FireTile,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }

    public class TemplateInformation
    {
        private readonly Dictionary<Vector3, WorldItem> _Locations;

        private readonly float _MaxX;
        private readonly float _MaxZ;
        public TemplateInformation(Template template)
        {
            _Locations = template.GetComponent<Template>().GetLocations();
            _MaxX = _Locations.Keys.Max(location => location.x);
            _MaxZ = _Locations.Keys.Max(location => location.z);
        }

        public Dictionary<Vector3, WorldItem> GetLocations()
        {
            return _Locations;
        }

        public Vector3 GetOffset(Vector3 position)
        {
            var originLocation = GetTemplateOriginForLocation(position);
            var noise = Mathf.PerlinNoise(originLocation.x / WorldGenerator.TemplateSizeX / 100f + 0.05f, originLocation.z / WorldGenerator.TemplateSizeZ / 100f + 0.05f);
            Random.InitState((int)(noise * 100000f));
            var xOffset = Mathf.Floor(Random.Range(0, (WorldGenerator.TemplateSizeX - _MaxX) / 2f));
            var zOffset = Mathf.Floor(Random.Range(0, (WorldGenerator.TemplateSizeZ - _MaxZ) / 2f));
            return new Vector3(xOffset, 0f, zOffset) * 2f;
        }

        public Vector3 GetTemplateOriginForLocation(Vector3 location)
        {
            var roundedX = Mathf.Floor(location.x / WorldGenerator.TemplateSizeX) * WorldGenerator.TemplateSizeX;
            var roundedZ = Mathf.Floor(location.z / WorldGenerator.TemplateSizeZ) * WorldGenerator.TemplateSizeZ;
            return new Vector3(roundedX, 0f, roundedZ);
        }
    }
}
