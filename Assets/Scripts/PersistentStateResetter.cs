﻿namespace Assets.Scripts
{
    public class PersistentStateResetter : Singleton<PersistentStateResetter>
    {
        private void Reset()
        {
            Enemy.ResetPersistentState();
        }

        private void OnDestroy()
        {
            Reset();
        }
    }
}
