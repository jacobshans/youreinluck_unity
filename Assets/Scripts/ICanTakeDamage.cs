﻿namespace Assets.Scripts
{
    public interface ICanTakeDamage
    {
        void TakeDamage(float damage);
    }
}