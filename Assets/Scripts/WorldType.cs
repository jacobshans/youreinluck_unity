﻿namespace Assets.Scripts
{
    public enum WorldType
    {
        Ice,
        Swamp,
        Desert,
        Lava
    }
}