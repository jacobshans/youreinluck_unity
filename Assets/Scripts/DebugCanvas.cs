﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class DebugCanvas : MonoBehaviour
    {
        [SerializeField] private GameObject _DebugPanel;
        private Controls _Controls;
        private bool _Showing;

        protected void Awake()
        {
            _Controls = new Controls();
            _Controls.Main.DebugCanvas.performed += context => ToggleShow();
        }

        private void ToggleShow()
        {
            if (_Showing)
            {
                foreach (Transform child in _DebugPanel.transform)
                {
                    Destroy(child.gameObject);
                }
                Time.timeScale = 1f;
                _Showing = false;
                return;
            }

            Time.timeScale = 0f;
            var characterStateInformations = GetCharacterStateInformations();
            ShowPanelPerInformation(characterStateInformations);
            _Showing = true;
        }

        private void ShowPanelPerInformation(List<MonoBehaviour> characterStateInformations)
        {
            var informationStrings = new List<string>();
            characterStateInformations.ForEach(info =>
            {
                informationStrings.Add(GetInformationString(info));
            });
            informationStrings.GroupBy(info => info).ToList().ForEach(info =>
            {
                var debugPanelItem = (GameObject)Instantiate(Resources.Load("Prefabs/DebugPanelItem"), _DebugPanel.transform);
                debugPanelItem.GetComponentInChildren<Text>().text = info.Count() + " x " + info.Key;
            });
        }

        private List<MonoBehaviour> GetCharacterStateInformations()
        {
            return FindObjectsOfType<PlayerInput>().OfType<MonoBehaviour>().Concat(FindObjectsOfType<Enemy>()).ToList();
        }

        private string GetInformationString(MonoBehaviour informationGameObject)
        {
            var information = ((ICharacterStateInformationHolder)informationGameObject).GetCharacterStateInformation();
            var informationString = informationGameObject.GetType().Name + "\n------------\n";
            var myType = information.GetType();
            var props = new List<PropertyInfo>(myType.GetProperties());
            foreach (var prop in props)
            {
                var propValue = prop.GetValue(information, null);
                informationString += $"{prop.Name}: {propValue}\n";
            }

            return informationString;
        }

        private void OnDisable()
        {
            _Controls.Disable();
        }

        private void OnEnable()
        {
            _Controls.Enable();
        }
    }
}
