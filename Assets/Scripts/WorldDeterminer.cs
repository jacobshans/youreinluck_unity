﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class WorldDeterminer
    {
        private static Vector2 _Offset;
        private static float _Scale;
        public const float Size = 50f;

        public static WorldType GetWorldTypeForLocation(Vector3 location)
        {
            var noise = GetNoiseForPosition(location);

            if (noise > 3 / 4f) return WorldType.Ice;
            if (noise > 1 / 2f) return WorldType.Swamp;
            if (noise > 1 / 4f) return WorldType.Desert;
            return WorldType.Lava;
        }

        public static float GetNoiseForPosition(Vector3 location)
        {
            var position = new Vector2(location.x, location.z) * _Scale + _Offset;
            var position2 = new Vector2(location.x, location.z) * _Scale * 1.5f + _Offset;
            var noise = Mathf.PerlinNoise(position.x, position.y) + Mathf.PerlinNoise(position2.x, position2.y);
            return noise / 2;
        }

        public static bool ShouldPlaceTreeAtLocation(Vector3 location)
        {
            var possibility = GetWorldTypeForLocation(location) switch
            {
                WorldType.Ice => 0.1f,
                WorldType.Swamp => 0.2f,
                WorldType.Desert => 0.02f,
                WorldType.Lava => 0.02f,
                _ => throw new ArgumentOutOfRangeException()
            };
            Random.InitState((int)(location.x * location.z));
            return Random.Range(0f, 1f) < possibility;
        }

        public static bool ShouldPlaceBushAtLocation(Vector3 location)
        {
            var possibility = GetWorldTypeForLocation(location) switch
            {
                WorldType.Ice => 0.1f,
                WorldType.Swamp => 0.02f,
                WorldType.Desert => 0.05f,
                WorldType.Lava => 0.02f,
                _ => throw new ArgumentOutOfRangeException()
            };
            Random.InitState((int)(location.x * location.z));
            return Random.Range(0f, 1f) < possibility;
        }

        public static bool ShouldPlaceSmallEnemyAtLocation(Vector3 location)
        {
            Random.InitState((int)(location.x * location.z));
            return Random.Range(0f, 100f) > 99.2f;
        }

        public static float GetScaleForWorldItem(WorldItem worldItem, Vector3 location)
        {
            Random.InitState((int)(location.x * location.z));
            return worldItem switch
            {
                WorldItem.Tree => Random.Range(.5f, 1.5f),
                WorldItem.Bush => Random.Range(1f, 2f),
                WorldItem.Rock => Random.Range(.5f, 1.2f),
                WorldItem.XpPoints => 1f,
                WorldItem.Chest => 1f,
                WorldItem.SmallEnemy => 1f,
                _ => throw new ArgumentOutOfRangeException(nameof(worldItem), worldItem, null)
            };
        }

        public static Quaternion GetRotationForPositionAndWorldItem(Vector3 location, WorldItem worldItem)
        {
            if(worldItem == WorldItem.Rock) return Quaternion.identity;
            Random.InitState((int)(location.x * location.z));
            return Quaternion.AngleAxis(Random.Range(-180f, 180f), Vector3.up);
        }

        public static void SetScaleAndOffset(float scale, Vector2 offset)
        {
            _Scale = scale;
            _Offset = offset;
        }
    }
}