﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class XpManager : Singleton<XpManager>
    {
        private float _TotalXp;
        private float _XpNeededForNextModification = 100f;
        [SerializeField] private Slider _XpSlider;


        public void AddXp(float xp)
        {
            _TotalXp += xp;
            GoToNextLevelIfPossible();

            _XpSlider.value = _TotalXp / _XpNeededForNextModification;
        }

        public void AddRatioXp(float ratio)
        {
            _TotalXp += ratio * (_XpNeededForNextModification /2f);
            GoToNextLevelIfPossible();

            _XpSlider.value = _TotalXp / _XpNeededForNextModification;
        }

        private void GoToNextLevelIfPossible()
        {
            if (_TotalXp < _XpNeededForNextModification) return;
            PlayerInput.Instance.IncreaseLevel();
            ShowCanvasWithChoices();
            _TotalXp = 0f;
            _XpNeededForNextModification *= 2f;
            MessageKit.Post(MessageType.LevelIncreased);
        }

        private void ShowCanvasWithChoices()
        {
            Instantiate(Resources.Load("Prefabs/ModificationCanvas"));
        }
    }
}
